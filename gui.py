import sys
from os.path import normpath, expanduser

from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QListWidgetItem, QMessageBox
from PyQt5.QtCore import Qt, QTimer, QThread
from PyQt5 import uic

import pyqtgraph as pg
from modules import worker
from modules import device
from concurrent.futures import ThreadPoolExecutor
import time

DEFAULT_DIR = normpath(expanduser("~/Documents/Measurements/Multimeter"))

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.measurement_default = "DCV"  # default measurement init
        # self.measurement = "DCV"

        self.init_ui()

    def init_ui(self):
        uic.loadUi('mainwindow.ui', self)
        self.pB_refreshDevice.clicked.connect(self.get_device_list)
        self.pB_browse.clicked.connect(self._show_folder_dialog)
        self.pB_start.clicked.connect(self._on_start_button)

        self.pB_DCV.clicked.connect(lambda: self._on_measurement_type_change("DCV"))
        self.pB_DCI.clicked.connect(lambda: self._on_measurement_type_change("DCI"))
        self.pB_ACV.clicked.connect(lambda: self._on_measurement_type_change("ACV"))
        self.pB_ACI.clicked.connect(lambda: self._on_measurement_type_change("ACI"))
        self.pB_res.clicked.connect(lambda: self._on_measurement_type_change("RES"))
        self.pB_res4w.clicked.connect(lambda: self._on_measurement_type_change("RES4W"))
        self.pB_freq.clicked.connect(lambda: self._on_measurement_type_change("FREQ"))
        self.pB_period.clicked.connect(lambda: self._on_measurement_type_change("PERIOD"))
        self.pB_cont.clicked.connect(lambda: self._on_measurement_type_change("CONT"))
        self.pB_diode.clicked.connect(lambda: self._on_measurement_type_change("DIODE"))
        self.pB_temp.clicked.connect(lambda: self._on_measurement_type_change("TEMP"))

        self.cB_autorange.stateChanged.connect(self._on_autorange_change)
        self.listWidget_devices.itemChanged.connect(self._on_device_selection)

        self.cB_plot.stateChanged.connect(self._show_plot)
        self.label_directory.setText(DEFAULT_DIR)
        self.lE_delay.editingFinished.connect(self._update_delay)

        self.curve = self.plotwidget.plot()
        self.show()

    def _show_plot(self, state):
        if state:
            self.resize(1280, 700)
            self.plotwidget.show()
        else:
            self.plotwidget.hide()
            self.resize(565, 700)

    def _update_delay(self):
        self.worker.set_interval.emit(float(self.lE_delay.text()))

    def get_device_list(self):
        """
        Append devices list with addresses of Keithley instruments.
        """

        address_list = device.Device.scan_for_devices()
        for address in address_list:
            item = QListWidgetItem(address)                         # Create items and add checkbox to it
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)    #
            item.setCheckState(Qt.Unchecked)                        #
            self.listWidget_devices.addItem(item)

    def _on_device_selection(self, item):
        if item.checkState():  # only when item on list is checked check case
            if self.pB_start.text() == 'STOP':
                warning = QMessageBox()
                warning.setText("Finish measurement before selecting new device")
                warning.setStandardButtons(QMessageBox.Ok)
                warning.setIcon(QMessageBox.Critical)
                warning.exec()
                item.setCheckState(Qt.Unchecked)
                return
            if hasattr(self, 'thread') and hasattr(self, 'worker'):  # when new device is selected, close current thread
                self.finish_thread = True                            # to allow new device run in new background thread
                self.thread.quit()
                self.thread.wait()

            self.device = device.Device(item.text())
            self._on_measurement_type_change(self.measurement_default)
            for i in range(self.listWidget_devices.count()):    # deselect all other items but our item - app can handle
                item_ = self.listWidget_devices.item(i)         # measurements from only one device at once
                if item != item_:                               #
                    item_.setCheckState(Qt.Unchecked)           #
            self.thread, self.worker = self.create_worker_thread()


    def create_worker_thread(self):
        thread = QThread()
        w = worker.Worker(self.device)
        w.on_data_arrive.connect(self._update_labels)
        w.update_plot.connect(self._update_plot)
        thread.finished.connect(w.deleteLater)
        w.moveToThread(thread)
        thread.start()
        w.start.emit()
        return thread, w

    def _update_plot(self, x, y):
        if self.plotwidget.isVisible():
            self.curve.setData(x, y)

    def _update_labels(self, value, time):
        self.label_measurement.setText(str(value))
        self.label_time.setText(str(time))

    def _show_folder_dialog(self):
        dialog = QFileDialog(self, "Measurement Directory")
        path = normpath(dialog.getExistingDirectory(self, "Select Measurement directory"))
        self.label_directory.setText(path)

    def _on_start_button(self):
        if self.pB_start.text() == 'START':
            params = {"save": True, "path": self.label_directory.text(), "sampleID": self.lE_sampleID.text(),
                      "delay": float(self.lE_delay.text())}
            self.worker.save_data.emit(params)
            self.pB_start.setText("STOP")
        else:
            self.worker.save_data.emit(False)
            self.pB_start.setText("START")


    def _on_measurement_type_change(self, measure):
        units = {"DCV": "V", "DCI": "A", "ACV": "V", "ACI": "A", "RES": "Ohm", "RES4W": "Ohm",
                 "FREQ": "Hz", "CONT": "", "DIODE": "", "PERIOD": "", "TEMP": ["CEL", "KEL", "FAR"]}
        if measure == "TEMP":
            unit = units[measure][self.get_range()]
        else:
            unit = units[measure]
        self.label_measurement_type.setText(f"Measurement: {measure}, [{unit}]")
        self._update_plot_ax_label(unit)
        if self.device:
            self.device.set_measurement(measure, self.get_range())

    def _update_plot_ax_label(self, unit):
        self.plotwidget.getAxis('left').setLabel(unit)
        self.plotwidget.getAxis('bottom').setLabel("s")

    def get_range(self):
        if self.cB_autorange.isChecked() or self.lE_range.text() == '':
            meas_range = 0
        else:
            meas_range = int(self.lE_range.text())
        return meas_range

    def _on_autorange_change(self, state):
        self.lE_range.setDisabled(state)

    def closeEvent(self, event):
        """ Override default PyQt's closeEvent, so there's prompt on exit now """
        quit_msg = "Are you sure you want to exit the program?"
        reply = QMessageBox.question(self, 'Message',
                                           quit_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

def main():
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()