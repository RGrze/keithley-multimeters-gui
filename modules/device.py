from PyQt5.QtCore import QObject, pyqtSignal
import visa

class Device(QObject):
    def __init__(self, address=None):
        super(Device, self).__init__()
        if address:
            self.d = self.connect(address)
            self.functions = {"DCV": "VOLTAGE:DC", "ACV": "VOLTAGE:AC", "DCI": "CURRENT:DC", "ACI": "CURRENT:AC",
                              "RES": "RESISTANCE", "RES4W": "FRESISTANCE", "FREQ": "FREQUENCY", "PERIOD": "PERIOD",
                              "CONT": "CONTINUITY", "DIODE": "DIODE", "TEMP": "TEMPERATURE"}
        else:
            self.d = None

    def __del__(self):
        if self.d:
            self.d.close()

    def connect(self, address):
        rm = visa.ResourceManager()
        return rm.open_resource(address)

    def set_measurement(self, measurement, range):
        self.write(f'SENSE:FUNCTION "{self.functions[measurement]}"')
        range_excluded = ("CONT", "DIODE", "TEMP")
        if measurement not in range_excluded:
            if not range:
                self.write(f"SENSE:{self.functions[measurement]}:RANGE:AUTO ON")
            else:
                self.write(f"SENSE:{self.functions[measurement]}:RANGE {range}")
        if measurement == "TEMP":
            if range == 0: unit = "Cel"
            elif range == 1: unit = "K"
            else: unit = "Far"
            self.write(f"SENSE:{self.functions[measurement]}:UNIT {unit}")

    def write(self, command):
        return self.d.write(command)

    def query(self, command):
        return self.d.query(command)

    @staticmethod
    def scan_for_devices():
        rm = visa.ResourceManager()
        addresses = []
        for adr in rm.list_resources():
            dev = rm.open_resource(adr)
            try:
                ID = dev.query("*IDN?")
            except visa.VisaIOError:
                ID = ""
                continue
            finally:
                print(ID)
                if 'KEITHLEY INSTRUMENTS' in ID:
                    addresses.append(adr)
                dev.close()
        return addresses