import os
import time
from datetime import datetime
from PyQt5.QtCore import QObject, QRunnable, pyqtSlot, pyqtSignal, QTimer


class Worker(QObject):
    start = pyqtSignal()
    stop = pyqtSignal()
    finished = pyqtSignal()
    set_interval = pyqtSignal(float)
    on_data_arrive = pyqtSignal(float, float)
    update_plot = pyqtSignal(list, list)

    save_data = pyqtSignal(object)

    def __init__(self, device):
        super(Worker, self).__init__()
        self.device = device

        self.start.connect(self.run)
        self.stop.connect(self._stop)
        self.save_data.connect(self._save_data)
        self.set_interval.connect(self._set_interval)

        self.delay = 0.1
        self._to_file = False
        self.finish_thread = False

        self.x = []
        self.y = []


    @pyqtSlot(float)
    def _set_interval(self, delay):
        self.timer.setInterval(int(delay*1000))

    @pyqtSlot()
    def run(self):
        self.timer = QTimer()
        self.timer.timeout.connect(self._background_task)
        if self.sender == 'pB_start':
            self.path = create_file(self.path, self.sampleID)
        self.start_time = time.time()
        self.timer.start(int(self.delay*1000))

    def _background_task(self):
        value = float(self.device.query("READ?"))
        t = time.time() - self.start_time
        if self._to_file:
            write(self.path, f"{value}\t{t}\n")
        else:
            if len(self.y) > 1000:
                self.x.clear()
                self.y.clear()
        self.x.append(t)
        self.y.append(value)
        self.on_data_arrive.emit(value, t)
        self.update_plot.emit(self.x, self.y)
        if self.finish_thread:
            self._stop()

    @pyqtSlot()
    def _stop(self):
        self.timer.stop()

    @pyqtSlot(object)
    def _save_data(self, x):
        if not x:
            self._to_file = False
        else:
            self.x.clear()
            self.y.clear()
            self.path = create_file(x["path"], x["sampleID"])
            write(self.path, "Measured value\tTime\n")
            self._set_interval(x["delay"])
            self.start_time = time.time()
            self._to_file = True

def create_file(path, sample):
    date = datetime.strftime(datetime.now(), "%Y.%m.%d-%H.%M.%S")
    filename = f"{date}_{sample}.csv"
    if not os.path.isdir(path):
        os.makedirs(path)
    open(os.path.join(path, filename), 'w').close()
    return os.path.join(path, filename)

def write(path, line):
    with open(path, 'a') as f:
        f.write(line)

